<?php

class Base
{
    public $createid;
    public $createdate;
    public $updateid;
    public $updatedate;
    
    public function __construct()
    {
        $this->createid = 'Admin';
        $this->createdate = date('Y-m-d H:i:s');
        $this->updateid = 'Admin';
        $this->updatedate = date('Y-m-d H:i:s');
    }
}