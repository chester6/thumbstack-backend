<?php

class User extends \Base
{
	protected $_db;
	public function __construct() {
        $this->_db = DBConnection::getConnection();
    }

    public function loginUser($request) {
        $Email =$request->getParam("Email");
        $Password = $request->getParam("Password");
		try{
            $sql = "SELECT *
                    FROM m_users
                    WHERE Email = :Email
                    AND Password = :Password";
			$stmt = $this->_db->prepare($sql);
            $stmt->bindParam(":Email", $Email);
            $stmt->bindParam(":Password", $Password);
			$stmt->execute();
			$query = $stmt->fetch(\PDO::FETCH_ASSOC);
			$data = $query;
	    	return $data; 
	    } catch(PDOException $e){
			echo "Error: ".$e->getMessage();
		}
    }
    
    public function add($request) {
		$Username = $request->getParam("Username");
        $FirstName = $request->getParam("FirstName");
        $LastName = $request->getParam("LastName");
        $Email = $request->getParam("Email");
        $Password = $request->getParam("Password");
        $UserPhoto = $request->getParam("UserPhoto");
        $IsDeleted = false;
        $base = new Base();
		
		try{

			$sql = "SELECT	*
					FROM	m_users
					WHERE	Username = :Username";
			$stmt = $this->_db->prepare($sql);
			$stmt->bindParam(":Username", $Username);
			$stmt->execute();
			$query = $stmt->fetchObject();

			if($query) {
				$data["status"] = "Error: Your account cannot be created at this time. Please try again later.";
			} else {
				// Gets the user into the database
				$sql = "INSERT INTO	m_users (Username, FirstName, LastName, Email, Password , UserPhoto, IsDeleted)
						VALUES		(:Username, :FirstName, :LastName, :Email, :Password , :UserPhoto, :IsDeleted)";

				$stmt = $this->_db->prepare($sql);

				$stmt->bindParam(":Username", $Username);
				$stmt->bindParam(":FirstName", $FirstName);
                $stmt->bindParam(":LastName", $LastName);
                $stmt->bindParam(":Email", $Email);
                $stmt->bindParam(":Password", $Password);
                $stmt->bindParam(":UserPhoto", $UserPhoto);
                $stmt->bindParam(":IsDeleted", $IsDeleted);
                

				$stmt->execute();
				$result = $this->_db->lastInsertId();
				if ($result) {
					$data["status"] = "Your account has been successfully created.";
				} else {
					$data["status"] = "Error: Your account cannot be create at this time. Please try again later.";
				}
			}
    
    	return $data; 
    } catch(PDOException $e) {
			echo "Error: ".$e->getMessage();
		}
    }
    
    public function update($request) {
        $id = $request->getParam("id");
        $Username = $request->getParam("Username");
        $FirstName = $request->getParam("FirstName");
        $LastName = $request->getParam("LastName");
        $Email = $request->getParam("Email");
        $Password = $request->getParam("Password");
        $UserPhoto = $request->getParam("UserPhoto");
        $IsDeleted = false;
        $base = new Base();

		try{
			$sql = "UPDATE	m_users
					SET name = :name, 
                    Username = :Username, 
                    FirstName = :FirstName, 
                    LastName = :LastName 
                    Password = :Password 
                    Email = :Email 
					WHERE	id = :id";
			$stmt = $this->_db->prepare($sql);
			
            $stmt->bindParam(":Username", $Username);
            $stmt->bindParam(":FirstName", $FirstName);
            $stmt->bindParam(":LastName", $LastName);
            $stmt->bindParam(":Email", $Email);
            $stmt->bindParam(":Password", $Password);
            $stmt->bindParam(":UserPhoto", $UserPhoto);
            $stmt->bindParam(":IsDeleted", $IsDeleted);
            $stmt->bindParam(":id", $id);
			$result = $stmt->execute();
			if ($result) {
				$data["status"] = "Your account has been successfully updated.";
			} else {
				$data["status"] = "Error: Your account cannot be updated at this time. Please try again later.";
			}
    	return $data; 
    	} catch(PDOException $e) {
			echo "Error: ".$e->getMessage();
		}
    
	}

    public function getList($request) {
		$token = bin2hex(openssl_random_pseudo_bytes(16));
		try{
			$sql = "SELECT *
					FROM m_users
					ORDER BY id DESC";
			$stmt = $this->_db->prepare($sql);
			$stmt->execute();
			$query = $stmt->fetchAll();
			$data = $query;
    	return $data; 
    	} catch(PDOException $e) {
			echo "Error: ".$e->getMessage();
		}
    
	}
 
    public function getDetails($request) {
		$id = $request->getAttribute("id");
		try{
            $sql = "SELECT *
                    FROM m_users
					WHERE	id = :id";
			$stmt = $this->_db->prepare($sql);
			$stmt->bindParam(":id", $id);
			$stmt->execute();
			$query = $stmt->fetch(\PDO::FETCH_ASSOC);
			$data = $query;
	    	return $data; 
	    } catch(PDOException $e){
			echo "Error: ".$e->getMessage();
		}
    
	}

	public function delete($request) {
		$id = $request->getAttribute("id");
		try{
			// Delete the quote
			$sql = "DELETE FROM	m_users
					WHERE id = :id";
			$stmt = $this->_db->prepare($sql);
			$stmt->bindParam(":id", $id);
			$result = $stmt->execute();
			if ($result) {
				$data["status"] = "Your account has been successfully deleted.";
			} else {
				$data["status"] = "Error: Your account cannot be delete at this time. Please try again later.";
			}
			return $data; 
		} catch(PDOException $e){
			echo "Error: ".$e->getMessage();
		}
	}
}
