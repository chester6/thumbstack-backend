<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// Import Monolog classes into the global namespace
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$container = $app->getContainer();

$container["logger"] = function ($c) {
	// create a log channel
	$log = new Logger("api");
	$log->pushHandler(new StreamHandler(__DIR__ . "/logs/app.log", Logger::INFO));

	return $log;
};


	$app->add(function (Request $request, Response $response, $next) {
		$response = $next($request, $response);
		// Access-Control-Allow-Origin: <domain>, ... | *
		$response = $response->withHeader('Access-Control-Allow-Origin', '*')
			->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
			->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
		return $response;
	});
	################### USER ###########################################

	$app->post("/Login", function (Request $request, Response $response) {
		$user = new User();	
		$data = $user->loginUser($request);
		return json_encode(["success" => true, "data" => $data]);
	});

	$app->post("/user/add", function (Request $request, Response $response) {	
		$user = new User();	
		$data = $user->Add($request);
		return json_encode(["success" => true, "data" => $data]);
	});

	$app->post("/user/update", function (Request $request, Response $response) {
		$user = new User();	
		$data = $user->update($request);
		return json_encode(["success" => true, "data" => $data]);
	});

	$app->get("/user/List", function (Request $request, Response $response) {
		$user = new User();	
		$data = $user->getList($request);
		return json_encode(["success" => true, "data" => $data]);
	});

	$app->get("/user/{id}", function (Request $request, Response $response) {	
		$user = new User();	
		$data = $user->getDetails($request);
		return json_encode(["success" => true, "data" => $data]);
	});

	$app->get("/user/delete/{id}", function (Request $request, Response $response) {
		$user = new User();
		$data = $user->delete($request);
		return json_encode(["success" => true, "data" => $data]);
	});

	################### END USER ###########################################

?>
